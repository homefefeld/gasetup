#!/bin/bash

configure() {
  su $GA_USER <<EOF
mkdir -p "$GA_ROMS"/mame "$GA_CONFIGS"/mame/{artwork,bgfx,cfg,cheat,crosshair,comments,ctrlr,diff,hash,ini,inp,language,nvram,samples,snap,sta,roms,software,dat}
cp hiscore.ini "$GA_CONFIGS"/mame
cd "$GA_CONFIGS"/mame/
/usr/lib/mame/groovymame \
    -artpath '~/shared/configs/mame/artwork;/usr/lib/mame/artwork' \
    -bgfx_path '~/shared/configs/mame/bgfx;/usr/lib/mame/bgfx' \
    -cfg_directory '~/shared/configs/mame/cfg' \
    -cheatpath '~/shared/configs/mame/cheat' \
    -comment_directory '~/shared/configs/mame/comments' \
    -crosshairpath '~/shared/configs/mame/crosshair' \
    -ctrlrpath '~/shared/configs/mame/ctrlr;/usr/lib/mame/ctrlr' \
    -diff_directory '~/shared/configs/mame/diff' \
    -fontpath '/usr/lib/mame/fonts;~/shared/frontend/fonts;/usr/share/fonts/TTF/' \
    -hashpath '~/shared/configs/mame/hash;/usr/lib/mame/hash' \
    -inipath '~/shared/configs/mame;~/shared/configs/mame/ini' \
    -input_directory '~/shared/configs/mame/inp' \
    -languagepath '~/shared/configs/mame/language;/usr/lib/mame/language' \
    -nvram_directory '~/shared/configs/mame/nvram' \
    -pluginspath '/usr/lib/mame/plugins;~/shared/configs/mame/plugins' \
    -samplepath '~/shared/configs/mame/samples' \
    -snapshot_directory '~/shared/configs/mame/snap' \
    -state_directory '~/shared/configs/mame/sta' \
    -rompath '~/shared/roms/mame' \
    -swpath '~/shared/configs/mame/software' \
    -plugin 'hiscore' \
    -skip_gameinfo '1' \
    -uifont 'Hack-Regular.ttf' \
    -video opengl \
    -lowlatency 1 \
    -modesetting 1 \
    -sound portaudio \
    -createconfig
EOF
mame_ini_config
ui_ini_config

su $GA_USER <<EOF
ln -s shared/configs/mame "$GA_HOME"/.mame
EOF

attract_config

# Add RGM as a frontend
[[ ! -e "$GA_FRONTENDS"/groovymame ]] && ln -s ../configs/mame "$GA_FRONTENDS"/groovymame
chown "$GA_USER":"$GA_GROUP" "$GA_FRONTENDS"/groovymame
}


update() {
  gm_version="$(groovymame -h | grep -oE "v0.[0-9]{3}")"
  # Let's not just overwrite config for versions other thn GM 227
  if [[ $(vercmp "$gm_version" "v.0227") == 0 ]] ; then
    mame_ini_config
  fi
}

mame_ini_config() {
su $GA_USER <<EOF
cd "$GA_CONFIGS"/mame/
/usr/lib/mame/groovymame \
    -artpath '~/shared/configs/mame/artwork;/usr/lib/mame/artwork' \
    -bgfx_path '~/shared/configs/mame/bgfx;/usr/lib/mame/bgfx' \
    -cfg_directory '~/shared/configs/mame/cfg' \
    -cheatpath '~/shared/configs/mame/cheat' \
    -comment_directory '~/shared/configs/mame/comments' \
    -crosshairpath '~/shared/configs/mame/crosshair' \
    -ctrlrpath '~/shared/configs/mame/ctrlr;/usr/lib/mame/ctrlr' \
    -diff_directory '~/shared/configs/mame/diff' \
    -fontpath '/usr/lib/mame/fonts;~/shared/frontend/fonts;/usr/share/fonts/TTF/' \
    -hashpath '~/shared/configs/mame/hash;/usr/lib/mame/hash' \
    -inipath '~/shared/configs/mame;~/shared/configs/mame/ini' \
    -input_directory '~/shared/configs/mame/inp' \
    -languagepath '~/shared/configs/mame/language;/usr/lib/mame/language' \
    -nvram_directory '~/shared/configs/mame/nvram' \
    -pluginspath '/usr/lib/mame/plugins;~/shared/configs/mame/plugins' \
    -samplepath '~/shared/configs/mame/samples' \
    -snapshot_directory '~/shared/configs/mame/snap' \
    -state_directory '~/shared/configs/mame/sta' \
    -rompath '~/shared/roms/mame' \
    -swpath '~/shared/configs/mame/software' \
    -plugin 'hiscore' \
    -skip_gameinfo '1' \
    -uifont 'Hack-Regular.ttf' \
    -video opengl \
    -lowlatency 1 \
    -modesetting 1 \
    -sound portaudio \
    -createconfig
EOF
}

ui_ini_config() {
su $GA_USER <<EOF
source /opt/gatools/include/includes.sh
set_mame_config_value ~/shared/configs/mame/ui.ini infos_text_size "1.00"
set_mame_config_value ~/shared/configs/mame/ui.ini font_rows "19"
set_mame_config_value ~/shared/configs/mame/ui.ini historypath '~/shared/configs/mame/dat\:/usr/share/mame'
set_mame_config_value ~/shared/configs/mame/plugin.ini hiscore "1"
EOF
}

attract_config() {
  if ! pacman -Q attract >/dev/null ; then
    echo "Attract Mode not installed, groovyarcade won't be installed"
    return
  fi
  echo "Installing groovymame for Attract Mode"
  sed -riE 's!([[:space:]]*param[[:space:]]+dat_path).*!\1 /usr/share/mame/dat/history.dat!' "$GA_FRONTENDS"/attract/attract.cfg
  #install -m 644 -o "$GA_USER" -g "$GA_GROUP" MAME.cfg "$GA_FRONTENDS"/attract/emulators/
  /opt/gatools/efc/efc.sh attract MAME.efc
}
