#!/bin/bash

configure() {
  su $GA_USER <<EOF
  install -d "$GA_SHARED" "$GA_CONFIGS" "$GA_FRONTENDS" "$GA_ROMS" "$GA_SHARED"/logs "$GA_HOME"/.config/openbox $MOTHER_OF_ALL/media

  touch "$GA_CONFIGS"/ga.conf
  touch "$GA_CONFIGS"/switchres.conf

  ln -s "$GA_CONFIGS"/ga.conf "$GA_HOME"/.config/ga.conf
  ln -s "$GA_CONFIGS"/switchres.conf "$GA_HOME"/switchres.conf

  cp .xinitrc "$GA_HOME"

  cp -a /etc/xdg/openbox/. "$GA_HOME"/.config/openbox/
  xmlstarlet ed -L -N s=http://openbox.org/3.4/rc -d "/s:openbox_config/s:keyboard/s:keybind[contains(@key, 'C-A') or contains(@key, 'S-A') or contains(@key, 'A-space')]" "$GA_HOME"/.config/openbox/rc.xml
EOF
}

update() {
  install -m 644 -o "$GA_USER" -g "$GA_GROUP" .xinitrc "$GA_HOME"/.xinitrc
  install -m 644 -o "$GA_USER" -g "$GA_GROUP" .bash_profile "$GA_HOME"/.bash_profile
}
