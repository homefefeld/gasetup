#!/bin/bash

configure() {
  dest="$GA_FRONTENDS"/attract
  su $GA_USER <<EOF
    install -dm775 "$dest"
    cp -R /usr/share/attract/* "$dest"
    cp attract.cfg "$dest"
    ln -s "$dest" "$GA_HOME"/.attract
EOF
}
