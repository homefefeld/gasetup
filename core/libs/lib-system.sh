#!/bin/bash

# gatools should already be included. This shell is not made to be called as a standalone

LIB_SYSTEM=1


worker_system_packages_menu() {
  local default=1
  local monitor
  # shellcheck disable=SC2034
  VIDEO_SETUP=1
  while true ; do
    # Keeping only options that are useful and have been tested
    ask_option $default "Softwares management" "Select an option" required \
      1 "Install/Uninstall" \
      2 "GroovyArcade specific Update" \
      3 "Full OS update" \
      4 "Enable/Disable testing repo" \
      9 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") execute worker system_packages_install ;;
      "2") execute worker system_packages_update_ga_only ;;
      "3") execute worker system_full_update ;;
      "4") execute worker manage_groovyarcade_testing_repo ;;
      "9") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
    # Sub menus will have their own $ANSWER_OPTION, but this variable is global
    # So $ANSWER_OPTION may not be a number and make an error here
    default=$((ANSWER_OPTION + 1))
  done

  return 0
}


update_pacman_db() {
  inform "Updating packages database"
  sudo pacman -Sy &>/dev/null
  if [[ $? != 0 ]] ; then
    inform "Couldn't update the package database. Aborting."
    sleep 2
    return 1
  fi
  return 0
}


worker_system_packages_install() {
  # 1. List existing packages
  # 2. set which ones are installed
  # 3. Ask user
  inform "Coming soon!"
  sleep 2
  return 0
}

# Let's not use this one for now, it lists GA specific updatable packages and lets
# the user decide which ones to update. The'yll make errors for sure, so I keep
# the code for now as it is useful, but not for that purpose yet
worker_system_packages_update_checklist() {
  # 1. update pacman
  # 2. copmpare the list of updatable packages against groovy installed packages
  # 3. ask user

  #
  # 1. Inform the user we're updating the pacman DB
  #
  update_pacman_db ||return 1

  #
  # 2. compare the list of updatable packages against groovy installed packages
  #
  local repo=groovyarcade
  local installed_pkgs=
  local updatable_pkgs=()

  # Check if the testing repo is active
  grep -qE '^\[groovyarcade-testing]$' /etc/pacman.d/groovy-ux-repo.conf && repo="groovyarcade groovyarcade-testing"
  for r in $repo ; do
    # Line example:
    # groovyarcade groovymame 0.227-1 [installed: 0.228-1]
    # |->repo      |-> package
    installed_pkgs="$(LANG=C pacman -Sl "$r" | grep "installed: " | cut -d ' ' -f2) $installed_pkgs"
  done

  # sort and deduplicate
  installed_pkgs="$(echo "$installed_pkgs" | sort | uniq)"
  # If the list is empty, inform and leave
  if [[ -z $installed_pkgs ]] ; then
    inform "No package to update"
    sleep 2
    return 0
  fi

  # Select updatable packages
  IFS=$'\n'
  for f in $(pacman -Qu) ; do
    # Check if the current package is in our list
    p="$(echo $f | cut -d ' ' -f1)"
    details="$(echo $f | cut -d ' ' -f2-)"
    echo "$installed_pkgs" | grep -q "$p" || continue
    updatable_pkgs+=($p "$details" OFF)
  done
  unset IFS

  #
  # 3. Display the list
  #
  ask_checklist "Select packages to update" 0 "${updatable_pkgs[@]}"
  log_info "About to update ${ANSWER_CHECKLIST[@]}"
  [[ -z ${ANSWER_CHECKLIST[@]} ]] && return 1
  sudo pacman -S --noconfirm ${ANSWER_CHECKLIST[@]}
  if [[ $? != 0 ]] ; then
    log_ko "Update failed"
    inform "Something went wrong when updating. Aborting."
    sleep 2
    return 1
  fi
  log_ok "Update succeeded"
  return 0
}


worker_system_packages_update_ga_only() {
  # 1. update pacman
  # 2. copmpare the list of updatable packages against groovy installed packages
  # 3. prompt user

  #
  # 1. Inform the user we're updating the pacman DB
  #
  update_pacman_db ||return 1

  #
  # 2. compare the list of updatable packages against groovy installed packages
  #
  local repo=groovyarcade
  local installed_pkgs=
  local updatable_pkgs=()

  # Check if the testing repo is active
  if grep -qE '^\[groovyarcade-testing]$' /etc/pacman.d/groovy-ux-repo.conf ; then
    inform "The GroovyArcade testing repository is enabled. Only update from it."
    sleep 2
    repo="groovyarcade-testing"
  fi

  # Get the list of updatable packages
  # Line example:
  # groovyarcade groovymame 0.227-1 [installed: 0.228-1]
  # |->repo      |-> package
  installed_pkgs="$(LANG=C pacman -Sl "$repo" | grep "installed: " | cut -d ' ' -f2)"

  # sort and deduplicate
  installed_pkgs="$(echo "$installed_pkgs" | sort | uniq)"
  # If the list is empty, inform and leave
  if [[ -z $installed_pkgs ]] ; then
    inform "No package to update"
    sleep 2
    return 0
  fi

  update_groovyarcade $installed_pkgs
  return $?
}


worker_system_full_update() {
  update_pacman_db ||return 1

  if ! sudo pacman -Qu ; then
    inform "No package to update"
    sleep 2
    return 0
  fi
  update_groovyarcade

  return $?
}


worker_manage_groovyarcade_testing_repo() {
  local next_status=disable

  grep -o "#\[groovyarcade-testing\]$" /etc/pacman.d/groovy-ux-repo.conf && next_status=enable

  ask_yesno "Do you really want to $next_status the testing repo ? Use at your own risks" || return 1

  if [[ $next_status == "enable" ]] ; then
    # Enable
    sudo sed -Ei '1,3s/^#(.*)/\1/g' /etc/pacman.d/groovy-ux-repo.conf
  else
    # Disable
    sudo sed -Ei '1,3s/(.*)/#\1/g' /etc/pacman.d/groovy-ux-repo.conf
  fi
  return $?
}


worker_system_install_gnome_software() {
  # Warn that groovy specific packages are not listed there
  sudo pacman -S gnome-software-packagekit-plugin archlinux-appstream-data
}


update_groovyarcade() {
  notify "$(sudo pacman -Qu $@ | sort | uniq)" || return 2

  ask_yesno "Do you really want to update ?" || return 3

  local pacman_opts="-Su --noconfirm"

  [[ ! -z "$@" ]] && pacman_opts="-Sy --noconfirm $@"

  # First update the keyring, easy problem source to fix
  sudo pacman -Sy --needed --noconfirm archlinux-keyring

  if ! sudo pacman $pacman_opts ; then
    notify "You'll have to manually run 'sudo pacman -Syu $@' and answer questions. Grab your keyboard !"
    return 4
  fi

  # Fix FS#70469 as Arch is not doing anything
  sudo chmod +s /usr/lib/Xorg.wrap
  return 0
}
