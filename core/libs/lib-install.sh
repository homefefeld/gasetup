#!/bin/bash
ESP_PATH=/boot

# Returns the needed swap size in GB
# Based on https://anto.online/code/bash-script-to-create-a-swap-file-in-linux/
decide_swap_size() {
  #get available physical ram
  local availMemMb=$(grep MemTotal /proc/meminfo | awk '{print $2}')

  #convert from kb to mb to gb
  local gb=$(awk "BEGIN {print $availMemMb/1024/1204}")

  #round the number to nearest gb
  local gb=$(echo $gb | awk '{print ($0-int($0)<0.499)?int($0):int($0)+1}')

  if [[ $gb == 0 ]] ; then
    notify "Couldn't detect the memory size, aborting swap file creation"
    lok_ko "Failed getting RAM size for swap size"
    return 1;
  fi

  # This calculation is not taking into account hibernation ...
  if [[ $gb -le 2 ]];  then
    swapSizeGb=2
  elif [[ $gb -gt 2 && $gb -lt 32 ]] ; then
    let swapSizeGb=4+$gb-2
  elif [[ $gb -gt 32 ]] ; then
    let swapSizeGb=$gb
  fi
  echo "$swapSizeGb"
  return 0
}

# $1
uefi_install_bootloader() {
  local disk="$1"
  # 2 possible methods : EFISTUB and a usual bootloader (systemd-boot, syslinux etc ...)
  # EFISTUB : boots fast, but hard to change kernel parameters as it must be done through efibootmgr
  # systemd-boot: why not
  # syslinux: it has the advantage that it already works for BIOS
  log_info "Installing UEFI bootloader"
  uefi_install_bootloader_syslinux "$disk"
}


# Setup syslinux for booting
# Requires that $default_cmdline $kernel_video_params are setup earlier in the
# script, they are not passed as arguments
# $1: the disk where GA is installed (ex: /dev/sda)
uefi_install_bootloader_syslinux() {
  local disk="$1"
  local syslinux_cfg_file=/groovyarcade/"$ESP_PATH"/EFI/syslinux/syslinux.cfg

  log_info "Installing syslinux as EFI bootloader"

  ga_root="$(disk_device_partition $disk)3"
  disk_uuid="$(lsblk "$ga_root" -no uuid)"

  # Copy the required files
  sudo mkdir -p /groovyarcade/"$ESP_PATH"/EFI/syslinux
  sudo cp -r /usr/lib/syslinux/efi64/* /groovyarcade/"$ESP_PATH"/EFI/syslinux

  # Prepare the syslinux.cfg
  cat <<EOF > "$syslinux_cfg_file"
default arch
timeout 0
prompt 0
#UI vesamenu.c32
menu title Groovy Arcade Linux
menu background splash.png
label arch
menu label GroovyArcade
linux /vmlinuz-linux-15khz
append root=/dev/disk/by-uuid/$disk_uuid rw $default_cmdline $kernel_video_params
initrd /initramfs-linux-15khz.img
EOF

  # Setup the boot entry
  sudo efibootmgr --create --disk "$disk" --part 1 --loader /EFI/syslinux/syslinux.efi --label "GroovyArcade UEFI" --verbose
}


# Setup systemd-boot for booting -> STILL TO BE DONE
uefi_install_bootloader_systemdboot() {
  log_info "Installing systemd-boot as EFI bootloader"
  # Install systemd-boot
  # bootctl must be run in chroot mode
  #bootctl --esp-path=/boot --boot-path=/boot install
  # Create the configuration files
}


# Partition a disk for UEFI booting
# $1: the device (ex: /dev/sda)
uefi_partition_disk() {
  local disk="$1"
  # Some blockdevices need a p before a partition number
  # https://unix.stackexchange.com/a/500910 + next answer
  
  local swapsize=

  if ! swapsize="$(decide_swap_size)" ; then
    log_warn "Failed to get swap size, force it to 2G"
    swapsize=2
  fi

  wipefs --all --force $disk || return 1
  sgdisk $disk -o > /dev/null
  sgdisk $disk -n 1::+512MiB -t 1:C12A7328-F81F-11D2-BA4B-00A0C93EC93B  || return 1
  sgdisk $disk -n 2::+"$swapsize"GiB -t 2:0657FD6D-A4AB-43C4-84E5-0933C84B4F4F  || return 1
  sgdisk $disk -n 3 -t 3:0FC63DAF-8483-4772-8E79-3D69D8477DE4  || return 1
  return 0
}


bios_install_bootloader() {
  local disk="$1"
  local boot_path=/groovyarcade/boot
  local syslinux_path="$boot_path"/syslinux/
  local syslinux_cfg="$syslinux_path"/syslinux.cfg

  ga_root="$(disk_device_partition $disk)3"
  disk_uuid="$(lsblk "$ga_root" -no uuid)"
  sudo mkdir -p "$syslinux_path"
  cp /usr/lib/syslinux/bios/*.c32 "$syslinux_path"
  cp /run/archiso/bootmnt/syslinux/splash.png "$boot_path"
  cat <<EOF > "$syslinux_cfg"
default arch
timeout 0
prompt 0
#UI vesamenu.c32
menu title Groovy Arcade Linux
menu background splash.png
label arch
menu label GroovyArcade
linux /vmlinuz-linux-15khz
append root=/dev/disk/by-uuid/$disk_uuid rw $default_cmdline $kernel_video_params
initrd /initramfs-linux-15khz.img
EOF
}

# 
bios_partition_disk() {
  local disk="$1"
  local swapsize=

  wipefs --all --force "$disk"

  if ! swapsize="$(decide_swap_size)" ; then
    log_warn "Failed to get swap size, force it to 2G"
    swapsize=2
  fi

  BOOT=400

  BOOTNUM=1
  SWAPNUM=2
  ROOTNUM=3

  ## FDISK AUTO PARTITION START
  echo "
o
n
p
${BOOTNUM}

+${BOOT}M
t
0b
n
p
${SWAPNUM}

+${swapsize}G
t
2
82
n
p
${ROOTNUM}


a
1
w
q
" | fdisk "$disk"
  return $?
}


# This will help setting the device name with partitions
# ex: mmcblk0, nvme0n1 -> ned a p, which is not the case with sda
disk_device_partition() {
  local base_dev="$(basename $1)"
  local part_sep=""

  # Devices like nvme or eMMC need a p before the partition number
  # That's because the last char of such device is a digit
  # ex: mmcblk0, nvme0n1
  if [[ "$base_dev" =~ mmcblk[0-9]+ || \
      ! -e "/sys/class/block/${base_dev}/device/type" || \
      $(cat "/sys/class/block/${base_dev}/device/type") != 0 \
    ]]; then
    part_sep="p"
  fi
  echo "$1"$part_sep
}


# Auto partioin a disk device, UEFI or BIOS
# $1: the disk device (ex: /dev/sda)
autopartition_disk() {
  local disk="$1"
  local disk_p="$(disk_device_partition "$disk")"

  # Make sure the disk device is valid
  lsblk "$disk" &> /dev/null || return 1
  log_info "Preparing device $disk"
  wipefs --all --force "$disk"
  
  # Determine if we go GPT or MBR
  if [[ -d /sys/firmware/efi/efivars ]] ; then
    log_info "This system was booted on UEFI, going the GPT way"
    uefi_partition_disk "$disk"
  else
    log_info "This system was booted on BIOS, going the MBR way"
    bios_partition_disk "$disk"
  fi
  if [[ $? != 0 ]]; then
    notify "Partitioning Error!!!"
    return 1
  fi
  inform "Partitioning Successful"
  partprobe "$disk"

  # Format parrtitions
  local bootpart="${disk_p}"1
  local swappart="${disk_p}"2
  local mainpart="${disk_p}"3
  mkfs.vfat -F32 $bootpart
  fatlabel "${bootpart}" GABOOT
  mkfs.ext4 -F -O ^64bit -q $mainpart
  e2label "${mainpart}" GA
  mkswap "$swappart"
  swapon "$swappart"
  return 0
}


auto_partition_and_mount_disk() {
  local disk="$1"
  local disk_p="$(disk_device_partition "$disk")"

  autopartition_disk "$disk" || return 1

  # This will turn to hell if there is already a GA install
  boot_dev="${disk_p}"1
  root_dev="${disk_p}"3

  [[ ! -d /groovyarcade ]] && mkdir /groovyarcade
  mount ${root_dev} /groovyarcade/

  [[ ! -d /groovyarcade/boot ]] &&  mkdir -p /groovyarcade/boot


  mount ${boot_dev} /groovyarcade/boot

  # Prepare the bootloader
  if [[ -d /sys/firmware/efi/efivars ]] ; then
    log_info "This system was booted on UEFI, installing the proper bootloader"
    uefi_install_bootloader "$disk"
  else
    bios_install_bootloader "$disk"
    log_info "This system was booted on BIOS, installing the proper bootloader"
  fi
}


worker_select_and_partition_disk() {
  local default=no
  sudo umount /media/* >/dev/tty12 2>&1
  local DEVICE=
  DISKS=$(for dev in $(lsblk -nde 2,7,11 -o NAME); do
    size=$(lsblk --output SIZE -n -d "/dev/$dev" | tr -d ' ')
    name=$(hwinfo --disk --only /dev/$dev | grep "Model:" | cut -d '"' -f 2 | tr " " "_")
    echo "/dev/$dev ($name/$size)"
  done)
  if [[ -z "$DISKS" ]]; then
    notify "No drives available to partition"
    return 1
  fi

  ask_option $default "Automatically Partition" "Choose a drive to auto-partition" required $DISKS \
  "Done" "-" || return 1
  echo $?

  [[ "$ANSWER_OPTION" == "Done" ]] && return 1

  DOIT=yes
  ask_yesno "Really auto-partition $ANSWER_OPTION drive?\nWarning! All data will be erased!!!" || DOIT=no
  [[ ! "$DOIT" == "yes" ]] && return 1
  DEVICE="$ANSWER_OPTION"
  inform "Autopartitioning $DEVICE..."
  log_info "Installing to $DEVICE"
  auto_partition_and_mount_disk "$DEVICE"
  INSTALL_DRIVE="$DEVICE"
  return $?
}
