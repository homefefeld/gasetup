#!/usr/bin/env bash

# DIALOG() taken from setup
# an el-cheapo dialog wrapper
#
# parameters: see dialog(1)
# returns: whatever dialog did
_zen_dialog() {
  zenity --title "$TITLE" "$@" \
    --window-icon=/usr/share/pixmaps/gasetup.png \
    --width=320 --height=240 \
    2> >(grep -v 'GtkDialog' >&2)
}

_zen_show_warning() {
  _zen_dialog --title "$1" \
    --info \
    --text="$2" \
    --icon-name="dialog-warning" \
    || die_error "zenity failed with arg3 $3"
}

_zen_notify() {
  # --notification creates a notification, this is not enough visible
  _zen_dialog --info \
    --text="$1" "${@:2}"
}

_zen_inform() {
  # $3 seems never used in the usual gasetup. Only used in core/libs/*.sh
  str="$1"
  if [ "$2" != 0 ]; then
    echo "$1" >>$LIBUI_DIA_SUCCESSIVE_ITEMS-$2
    str=$(cat $LIBUI_DIA_SUCCESSIVE_ITEMS-$2)
  fi
  [ "$3" = 1 ] && rm $LIBUI_DIA_SUCCESSIVE_ITEMS-$2
  _zen_dialog --info --text="$str"
}


_zen_ask_checklist() {
  _zen_notify "Check list not yet implemented in zenity UI" ; return 1
  str=$1
  elaborate=$2
  shift 2
  list=()
  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "no item given for element $1"
    [ -z "$3" ] && die_error "no ON/OFF switch given for element $1 (item $2)"
    [ "$3" != ON -a "$3" != OFF ] && die_error "element $1 (item $2) has status $3 instead of ON/OFF!"
    list+=("$1" "$2" $3)
    [ $elaborate -gt 0 ] && list+=("$4") # this can be an empty string, that's ok.
    shift 3
    [ $elaborate -gt 0 ] && shift
  done
  # i wish dialog would accept something like: --output-separator $'\0'
  # but it doesn't. there really is no good way to separate items currently
  # let's assume there are no newlines in the item tags
  ANSWER_CHECKLIST=()
  elab=''
  [ $elaborate -gt 0 ] && elab='--item-help'
  while read -r line; do
    ANSWER_CHECKLIST+=("$line")
  done < <(_zen_dialog --separate-output $elab --checklist "$str" 0 0 0 "${list[@]}")
  local ret=$?
  debug 'UI' "_zen_ask_checklist: user checked ON: ${ANSWER_CHECKLIST[@]}"
  return $ret
}

_zen_ask_datetime() {
  # display and ask to set date/time
  debug 'UI' "Date as specified by user $_date time: $_time"

  res=$(_zen_dialog --forms \
    --add-calendar="Date" --forms-date-format='%Y-%m-%d' \
    --add-combo="Hour" --combo-values="$(seq -s '|' 0 23)" \
    --add-combo="Minutes" --combo-values="$(seq -s '|' 0 59)" \
    --add-combo="Seconds" --combo-values="$(seq -s '|' 0 59)")
  date=$(echo $res | cut -d "|" -f 1)
  hour=$(echo $res | cut -d "|" -f 2)
  mins=$(echo $res | cut -d "|" -f 3)
  secs=$(echo $res | cut -d "|" -f 4)
  # Final format
  ANSWER_DATETIME=$(printf "%s %02d:%02d:%02d" "$date" $hour $mins $secs)
}

_zen_ask_number() {
  # If min and max values are set, use a scale dialog
  if [[ -n $2 ]] && [[ -n $3 ]] ; then
    opts="--scale --min-value=$2 --max-value=$3"
  else
    opts="--entry"
  fi
  ANSWER_NUMBER=$(_zen_dialog --text="$1" $opts)
  if [[ -n $ANSWER_NUMBER ]] && ! echo "$ANSWER_NUMBER" | grep -qE "^[[:digit:]]+$" ; then
    _zen_ask_number "$1" "$2" "$3"
  fi
  [[ -z "$ANSWER_NUMBER" ]] && return 1
  return 0
}

_zen_ask_option() {
  # $5 not handled, no clue on it either
  local list_opt=()
  local skip=1
  local val=
  for opt in "${@:5}" ; do
    if [[ $skip == 1 ]] ; then
      val="$opt"
      skip=0
      continue
    else
      skip=1
    fi
    if [[ "$opt" == "$1" ]] ; then
      list_opt+=(TRUE)
    else
      list_opt+=(FALSE)
    fi
    list_opt+=("$val $opt")
  done
  #echo $list_opt ; return 1
  ret=$(_zen_dialog --title "$2" \
    --list --radiolist --column "" --column "Select a value" \
    "${list_opt[@]}")
  for it in "${list_opt[@]}" ; do
    [[ "$it" == "$ret" ]] && ANSWER_OPTION=$(echo "$ret" | cut -d " " -f 1) && break
  done
  [[ $4 == "required" ]] && [[ -z $ret ]] && _zen_ask_option "$@"
  return 0 # TODO: check if dialog returned >0 because of an other reason then the user hitting 'cancel/skip'
}

_zen_ask_password() {
  # this is nowhere used in gasetup, $2 is undocumented, let's do our best
  if [ -n "$1" ]; then
    type_l=$(tr '[:upper:]' '[:lower:]' <<<$1)
    type_u=$(tr '[:lower:]' '[:upper:]' <<<$1)
  else
    type_l=
    type_u=
  fi

  local ANSWER=$(_zen_dialog --password --text "Enter your $type_l password")
  local ret=$?
  [ -n "$type_u" ] && read ${type_u}_PASSWORD <<<$ANSWER
  [ -z "$type_u" ] && PASSWORD=$ANSWER
  echo $ANSWER
  debug 'UI' "_zen_ask_password: user entered <<hidden>>"
  return $ret
}

_zen_ask_string() {
  exitcode=${3:-1}
  ANSWER_STRING=$(_zen_dialog --title "$3" --entry --text "$1" --entry-text="$2")
  local ret=$?
  debug 'UI' "_zen_ask_string: user entered $ANSWER_STRING"
  [ -z "$ANSWER_STRING" ] && return $exitcode
  return $ret
}

_zen_ask_string_multiple() {
  # Unused nowhere in the code as of now
  # ANSWER_VALUES seems not filled as of now
  MAXRESPONSE=0
  formtitle="$1"
  exitcode="${2:-1}"
  shift 2

  items=()

  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "No default value for $1"
    # format: Label Y X Value Y X display-size value-size
    items+=(--add-entry="$1")
    shift 2
  done
  res=$(_zen_dialog --forms "$formtitle" "${items[@]}")
  ANSWER_VALUES=()
  echo "$res" | tr "|" '\n' | while read -r item
  do
    ANSWER_VALUES+=($item)
  done
}

_zen_ask_yesno() {
  local default
  str=$1
  # If $2 contains an explicit 'no' we set defaultno for yesno dialog
  [ "$2" == "no" ] && default="--default-cancel"
  zenity --question $default --text="Are you sure, proceed to shutdown?"
  local ret=$?
  [ $ret -eq 0 ] && debug 'UI' "dia_ask_yesno: User picked YES"
  [ $ret -ne 0 ] && debug 'UI' "dia_ask_yesno: User picked NO"
  return $ret
}

_zen_follow_progress() {
  # Not used anywhere ...
  # What would be great, is that this tunctions is piped to the process which
  # would output its percentage progression
  title=$1
  logfile=$2

  _zen_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 >$LIBUI_FOLLOW_PID
  FOLLOW_PID=$(cat $LIBUI_FOLLOW_PID)
  rm $LIBUI_FOLLOW_PID

  # I wish something like this would work.  anyone who can explain me why it doesn't get's to be contributor of the month.
  # FOLLOW_PID=`_zen_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 2>&1 >/dev/null | head -n 1`

  # Also this doesn't work:
  # _zen_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 &>/dev/null &
  # FOLLOW_PID=$!

  # Also the new stdout-stderr-swapping isn't a clean solution for that. When command substitition is used bash will
  # wait until the command terminates, dialog's forking to background will fail.
  # FOLLOW_PID=$(_zen_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0)
}
