_cli_show_warning() {
  echo "WARNING: $1"
  [ "$3" = msg ] && echo -e "$2"
  [ "$3" = text ] && (cat $2 || die_error "Could not cat $2")
}

_cli_notify() {
  #echo -e "$@"
  _dia_dialog --msgbox "$@" 0 0
}

_cli_inform() {
  echo -e "$1"
}

_cli_ask_checklist() {
  str=$1
  elaborate=$2
  shift 2
  ANSWER_CHECKLIST=()
  adv=0
  if [ -n "$9" ]; then
    # if we have more then 2 elements, switch to advanced mode where we create a tmp file, and put all elements on a separate line
    # we open text editor where user can comment lines to disable items.  this is easier to navigate and manipulate.
    # to check for a 3rd element we use field 9. remember elaborate_expl field is optional, and when given can be zero-length.  so input could be one of:
    # t1 i1 ON|OFF '' t2 i2 ON|OFF '' t3 i3 ON|OFF ''
    # t1 i1 ON|OFF t2 i2 ON|OFF t3 i3 ON|OFF
    adv=1 && seteditor || return 1
    tmpfile=$(mktemp --tmpdir=$LIBUI_TMP_DIR _cli_ask_checklist-data.XXXX) || return 1
    echo "Comment sign in front of a line indicates OFF-setting." >>$tmpfile
    echo "Edit this file, when done, just exit the editor" >>$tmpfile
    echo >>$tmpfile
    allowed_tags=()
  fi
  declare -A defaults
  defaults['ON']=yes
  defaults['OFF']=no
  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "no item given for element $1"
    [ -z "$3" ] && die_error "no ON/OFF switch given for element $1 (item $2)"
    [ "$3" != ON -a "$3" != OFF ] && die_error "element $1 (item $2) has status $3 instead of ON/OFF!"
    item=$1
    elab=
    [ $elaborate -gt 0 ] && elab=$4
    [ "$2" != '-' -a "$2" != '^' ] && item="$1 ($2)"
    if [ $adv -eq 0 ]; then
      [ -n "$elab" ] && elab="\n$elab"
      ask_yesno "Enable $1 ?$elab" ${defaults[$3]} && ANSWER_CHECKLIST+=("$1")
    else
      allowed_tags+=($1)
      [ "$3" = OFF ] && echo -n '#' >>$tmpfile
      echo "$item $elab" >>$tmpfile
    fi
    shift 3
    [ $elaborate -gt 0 ] && shift
  done
  if [ $adv -eq 1 ]; then
    $EDITOR $tmpfile || return 1
    for i in $(grep -v ^# $tmpfile | cut -d' ' -f1); do
      check_is_in "$i" "${allowed_tags[@]}" && ANSWER_CHECKLIST+=("$i")
    done
  fi
  return 0
}

_cli_ask_datetime() {
  ask_string "Enter date [YYYY-MM-DD hh:mm:ss]"
  ANSWER_DATETIME=$ANSWER_STRING
  debug 'UI' "Date as picked by user: $ANSWER_STRING"
}

_cli_ask_number() {
  #TODO: i'm not entirely sure this works perfectly. what if user doesnt give anything or wants to abort?
  while true; do
    str="$1"
    [ -n $2 ] && str2="min $2"
    [ -n $3 -a $3 != '0' ] && str2="$str2 max $3"
    [ -n $4 ] && str2=" default $4"
    [ -n "$str2" ] && str="$str ( $str2 )"
    echo "$str"
    read ANSWER_NUMBER
    if [[ $ANSWER_NUMBER == *[^0-9]* ]]; then
      show_warning 'Invalid number input' "$ANSWER_NUMBER is not a number! try again."
    else
      if [ -n "$3" ] && [ $3 != '0' -a $ANSWER_NUMBER -gt $3 ]; then
        show_warning 'Invalid number input' "$ANSWER_NUMBER is bigger then the maximum,$3! try again."
      elif [ -n "$2" ] && [ $ANSWER_NUMBER -lt $2 ]; then
        show_warning 'Invalid number input' "$ANSWER_NUMBER is smaller then the minimum,$2! try again."
      else
        break
      fi
    fi
  done
  debug 'UI' "cli_ask_number: user entered: $ANSWER_NUMBER"
  [ -z "$ANSWER_NUMBER" ] && return 1
  return 0
}

_cli_ask_option() {
  #TODO: strip out color codes
  #TODO: if user entered incorrect choice, ask him again
  DEFAULT=
  [ "$1" != 'no' ] && DEFAULT=$1

  MENU_TITLE=$2
  EXTRA_INFO=$3
  shift 4

  echo "$MENU_TITLE"
  [ -n "$EXTRA_INFO" ] && echo "$EXTRA_INFO"
  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "ask_option error: tag $1 has no item"
    echo "$1 ] $2"
    shift 2
  done
  CANCEL_LABEL=CANCEL
  [ $TYPE == optional ] && CANCEL_LABEL=SKIP
  echo "$CANCEL_LABEL ] $CANCEL_LABEL"
  [ -n "$DEFAULT" ] && echo -n " > [ $DEFAULT ] "
  [ -z "$DEFAULT" ] && echo -n " > "
  read ANSWER_OPTION
  local ret=0
  [ -z "$ANSWER_OPTION" -a -n "$DEFAULT" ] && ANSWER_OPTION="$DEFAULT"
  [ "$ANSWER_OPTION" == CANCEL ] && ret=1 && ANSWER_OPTION=
  [ "$ANSWER_OPTION" == SKIP ] && ret=0 && ANSWER_OPTION=
  [ -z "$ANSWER_OPTION" -a "$TYPE" == required ] && ret=1

  debug 'UI' "cli_ask_option: ANSWER_OPTION: $ANSWER_OPTION, returncode (skip/cancel): $ret ($MENU_TITLE)"
  return $ret
}

_cli_ask_password() {
  if [ -n "$1" ]; then
    type_l=$(tr '[:upper:]' '[:lower:]' <<<$1)
    type_u=$(tr '[:lower:]' '[:upper:]' <<<$1)
  else
    type_l=
    type_u=
  fi

  echo -n "Enter your $type_l password: "
  stty -echo
  [ -n "$type_u" ] && read ${type_u}_PASSWORD
  [ -z "$type_u" ] && read PASSWORD
  stty echo
  echo
}

# $3 -z string behavior: always take default if applicable, but if no default then $3 is the returncode (1 is default)
_cli_ask_string() {
  exitcode=${3:-1}
  echo "$1: "
  [ -n "$2" ] && echo "(Press enter for default.  Default: $2)"
  echo -n ">"
  read ANSWER_STRING
  debug 'UI' "cli_ask_string: User entered: $ANSWER_STRING"
  if [ -z "$ANSWER_STRING" ]; then
    if [ -n "$2" ]; then
      ANSWER_STRING=$2
    else
      return $exitcode
    fi
  fi
  return 0
}

_cli_ask_string_multiple() {
  echo "$1"
  exitcode=${2:-1}
  shift 2

  ANSWER_VALUES=()
  i=0
  while [ $# -gt 0 ]; do
    _cli_ask_string "$1" "$2" $exitcode
    ANSWER_VALUES[$i]="$ANSWER_STRING"
    let i++
    shift 2
  done
}

_cli_ask_yesno() {
  [ -z "$2" ] && echo -ne "$1 (y/n): "
  [ "$2" = yes ] && echo -ne "$1 (Y/n): "
  [ "$2" = no ] && echo -ne "$1 (y/N): "

  read answer
  answer=$(tr '[:upper:]' '[:lower:]' <<<$answer)
  if [ "$answer" = y -o "$answer" = yes ] || [ -z "$answer" -a "$2" = yes ]; then
    debug 'UI' "cli_ask_yesno: User picked YES"
    return 0
  else
    debug 'UI' "cli_ask_yesno: User picked NO"
    return 1
  fi
}

_cli_follow_progress() {
  title=$1
  logfile=$2
  echo "Title: $1"
  [ -n "$3" ] && tail -f $2 --pid=$3
  [ -z "$3" ] && tail -f $2
}
